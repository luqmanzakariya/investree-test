// ===== 1. Check the string is palindrome or not =====
function isPallindrome(str){
  let reverse = str.split('').reverse().join('')
  if (reverse === str){
    console.log(true)
  }
  else {
    console.log(false)
  }
}
isPallindrome('abcba')

// ===== 2. Find prime number by range =====
function findPrimeByRange(bottom, upper){
  let arr = []
  for (let i=bottom; i<upper; i++){
    let flag = 0
    for (let j=2; j<i; j++){
      if (i%j === 0){
        flag = 1
      }
    }

    if (flag === 0){
      arr.push(i)
    }
  }
  console.log(arr)
}
findPrimeByRange(11, 40)


// ===== 3. Grouping array group into separate sub array group =====
const arr = ['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e']
function group (input){
  let arr = []
  let count = 0
  arr.push([input[0]])
  for (let i=1; i<input.length; i++){
    if (input[i] === input[i-1]){
      arr[count].push(input[i])
    }
    else {
      arr.push([])
      count++
      arr[count].push(input[i])
    }
  }
  console.log(arr)
}
group(arr)

// ===== 4. Count same element in an array with format =====
const arr2 = ['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e']
function count (input){
  let obj = {}
  for (let i=0; i<input.length; i++){
    if (obj[input[i]]=== undefined){
      obj[input[i]] = 1
    }
    else {
      obj[input[i]]++
    }
  }
  console.log(obj)
}
count(arr2)