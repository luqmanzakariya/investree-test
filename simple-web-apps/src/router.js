import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/reksadana',
      name: 'reksadana',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Reksadana.vue')
    },
    {
      path: '/sbn',
      name: 'sbn',
      component: () => import(/* webpackChunkName: "about" */ './views/Sbn.vue')
    },
    {
      path: '/osf',
      name: 'osf',
      component: () => import(/* webpackChunkName: "about" */ './views/Osf.vue')
    },
    {
      path: '/invoice/conventional',
      name: 'invoiceConventional',
      component: () => import(/* webpackChunkName: "about" */ './views/InvoiceConventional.vue')
    },
    {
      path: '/invoice/sharia',
      name: 'invoiceSharia',
      component: () => import(/* webpackChunkName: "about" */ './views/InvoiceSharia.vue')
    }
  ]
})
