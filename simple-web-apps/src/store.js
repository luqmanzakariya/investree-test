import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    invoiceConventional: [],
    invoiceSharia: [],
    homeCard: [],
    reksadana: [],
    sbn: [],
    osf: []
  },
  mutations: {
    GETINVOICECONVENTIONAL (state, payload) {
      state.invoiceConventional = payload
    },
    GETINVOICESHARIA (state, payload) {
      state.invoiceSharia = payload
    },
    GETHOMECARD (state, payload) {
      state.homeCard = payload
    },
    GETSBN (state, payload) {
      state.sbn = payload
    },
    GETOSF (state, payload) {
      state.osf = payload
    },
    GETREKSADANA (state, payload) {
      state.reksadana = payload
    }
  },
  actions: {
    getInvoiceConventional ({ commit }, payload) {
      commit('GETINVOICECONVENTIONAL', payload)
    },
    getInvoiceSharia ({ commit }, payload) {
      commit('GETINVOICESHARIA', payload)
    },
    getMainCard ({ commit }, payload) {
      commit('GETHOMECARD', payload)
    },
    getSbn ({ commit }, payload) {
      commit('GETSBN', payload)
    },
    getOsf ({ commit }, payload) {
      commit('GETOSF', payload)
    },
    getReksadana ({ commit }, payload) {
      commit('GETREKSADANA', payload)
    }
  }
})
