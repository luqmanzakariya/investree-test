# investree-test

## Simple Logic
To check the code :

    go to simple-logic folder and check test.js

## Simple Web App

To run the app :

    go to simple-web-apps folder
    run on terminal: npm run serve


Here's json file that I used:
1. Invoice conventional using json /invoice/conventional_osf.json
2. Invoice sharia using json /invoice/sharia_osf.json
3. OSF using json: /osf/sharia_osf.json karena osf.json tidak ditemukan dan memiliki file yang berbeda dengan di folder invoice. Referensi saya adalah main.json
4. SBN using json: /sbn/sbn.json
5. Reksadana using json: /reksadana/reksadana.json

